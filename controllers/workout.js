import Workout from "../models/Workout.js";

/**
 * @route POST /workouts/addWorkout
 * @desc Adds a worlout
 * @access Public
 */

export async function addWorkout(req, res) {
	// create an instance of a workout
	try {
		const userID = req.user._id;
		const { name, type, muscle, equipment, difficulty, instructions } =
			req.body;

		const existingWorkout = await Workout.findOne({ userID, name });
		if (existingWorkout) {
			return res
				.status(400)
				.json({ error: "Workout Already Exists in Database!" });
		}

		const workout = new Workout({
			userID,
			name,
			type,
			muscle,
			equipment,
			difficulty,
			instructions,
		});

		// save the workout to the database
		await workout.save();
		res.status(200).json(workout);
	} catch (error) {
		res.status(500).json(error);
	}
	res.end();
}
export async function getWorkouts(req, res) {
	try {
		const userID = req.user._id;
		const workouts = await Workout.find(
			{ userID },
			"-_id name type muscle equipment difficulty instructions"
		).exec();
		res.json(workouts);
	} catch (error) {
		res.status(500).json(error);
	}
	res.end();
}

export async function removeWorkout(req, res) {
	try {
		const { name, type, muscle, equipment, difficulty, instructions } =
			req.body;
		const userID = req.user._id;
		const workout = await Workout.findOneAndDelete({ userID, name }).exec();
		res.json(workout);
	} catch (error) {
		res.status(500).json(error);
	}
	res.end();
}
