import User from "../models/User.js";
import jwt from "jsonwebtoken";
import Blacklist from "../models/Blacklist.js";

import path from "path";
import { fileURLToPath } from "url";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const SECRET_ACCESS_TOKEN = process.env.SECRET_ACCESS_TOKEN;

export async function Verify(req, res, next) {
	const authHeader = req.headers["cookie"]; // get the session cookie from request header

	if (!authHeader) {
		res.sendFile(path.join(__dirname, "../public/html/login.html"));
		return;
	}
	const cookie = authHeader.split("=")[1]; // If there is, split the cookie string to get the actual jwt token
	const accessToken = cookie.split(";")[0];
	const checkIfBlacklisted = await Blacklist.findOne({ token: accessToken }); // Check if that token is blacklisted
	// if true, send an unathorized message, asking for a re-authentication.
	if (checkIfBlacklisted)
		return res
			.status(401)
			.json({ message: "This session has expired. Please login" });
	// if token has not been blacklisted, verify with jwt to see if it has been tampered with or not.
	// that's like checking the integrity of the accessToken
	jwt.verify(accessToken, SECRET_ACCESS_TOKEN, async (err, decoded) => {
		if (err) {
			// if token has been altered, return a forbidden error
			return res
				.status(401)
				.json({ message: "This session has expired. Please login" });
		}

		const { id } = decoded; // get user id from the decoded token
		const user = await User.findById(id); // find user by that `id`
		const { password, ...data } = user._doc; // return user object but the password
		req.user = data; // put the data object into req.user
		next();
	});
}

export default Verify = Verify;
