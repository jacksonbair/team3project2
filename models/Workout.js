import mongoose from "mongoose";

const WorkoutSchema = new mongoose.Schema(
	{
		userID: {
			type: String,
			required: "UserId is required",
			select: false,
			max: 25,
		},
		name: {
			type: String,
			required: "Name of workout required",
			max: 25,
		},
		type: {
			type: String,
			required: "Type of workout required",
			max: 25,
		},
		muscle: {
			type: String,
			required: "Muscle is required",
			max: 25,
		},
		equipment: {
			type: String,
			required: "Equipment is required",
			max: 25,
		},
		difficulty: {
			type: String,
			required: "Difficulty is required",
			max: 25,
		},
		instructions: {
			type: String,
			required: "Instructions are required",
		},
	},
	{ timestamps: true }
);

export default mongoose.model("workouts", WorkoutSchema);
