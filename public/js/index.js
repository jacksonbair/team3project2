// Simple index js stuff

document.addEventListener("DOMContentLoaded", async function () {
	const searchButton = document.getElementById("searchButton");

	searchButton.addEventListener("click", async function () {
		console.log("PRESSED");
		try {
			const name = document.getElementById("name_entry").value;
			const type = document.getElementById("type_entry").value;
			const muscle = document.getElementById("muscle_entry").value;
			const difficulty = document.getElementById("difficulty_entry").value;

			const queryParams = {
				name: name,
				type: type,
				muscle: muscle,
				difficulty: difficulty,
			};

			const response = await fetch("/workouts", {
				method: "POST",
				headers: { "Content-Type": "application/json" },
				body: JSON.stringify(queryParams),
			});
			// Log the response object
			console.log(response);

			// Extract JSON data from response body
			const data = await response.json();

			// Log the extracted data
			console.log(data);

			// Pass the data to printWorkoutQuery function
			printWorkoutQuery(data);

			if (!response.ok) {
				throw new Error("Failed to retrieve workouts");
			}
		} catch (error) {
			console.error(error);
		}
	});
});

function printWorkoutQuery(query) {
	let destination = document.getElementById("new-workout-display");

	let htmlContent = "";

	// console.log(query);
	for (let i = 0; i < query.length; i++) {
		let workout = query[i];

		// Create a div for each workout
		htmlContent += "<div class='workout'>";

		// Display workout details
		htmlContent +=
			"<p><strong>Workout Name:</strong> " + workout.name + "</p>" + "<br>";
		htmlContent +=
			"<p><strong>Workout Type:</strong> " + workout.type + "</p>" + "<br>";
		htmlContent +=
			"<p><strong>Muscle Group:</strong> " + workout.muscle + "</p>" + "<br>";
		htmlContent +=
			"<p><strong>Instructions:</strong> " +
			workout.instructions +
			"</p>" +
			"<br>";
		htmlContent +=
			"<p><strong>Equipment:</strong> " + workout.equipment + "</p>" + "<br>";
		htmlContent +=
			"<p><strong>Difficulty:</strong> " + workout.difficulty + "</p>" + "<br>";

		// Add a button for each workout
		htmlContent +=
			"<button onclick='addWorkout(" +
			JSON.stringify(workout) +
			")'>Add to Profile</button>";

		// Close the workout div
		htmlContent += "</div>";

		// Add a horizontal line between workouts
		htmlContent += "<hr>";
	}

	destination.innerHTML = htmlContent;
}

async function addWorkout(workout) {
	try {
		const response = await fetch("/workout/add", {
			method: "POST",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify(workout),
		});

		if (!response.ok) {
			throw new Error("Failed to Add Workout");
		}
	} catch (error) {
		console.error(error);
	}
}
