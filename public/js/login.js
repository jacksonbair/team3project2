/* ----- Login ----- */
document.addEventListener("DOMContentLoaded", async function () {
	const loginButton = document.getElementById("loginButton");

	loginButton.addEventListener("click", async function (event) {
		event.preventDefault();
		console.log("Login Attempted");
		try {
			const usernameAttempt = document.getElementById("username").value;
			const passwordAttempt = document.getElementById("password").value;

			const userData = {
				username: usernameAttempt,
				password: passwordAttempt,
			};

			const response = await fetch("/auth/login", {
				method: "POST",
				headers: { "Content-Type": "application/json" },
				body: JSON.stringify(userData),
			});

			if (!response.ok) {
				throw new Error("Failed to Login");
			}
			if (response.ok) {
				window.location = "../html/index.html";
			}
		} catch (error) {
			console.error(error);
		}
	});
});

/* ----- Register ----- */
document.addEventListener("DOMContentLoaded", async function () {
	const loginButton = document.getElementById("registerButton");

	loginButton.addEventListener("click", async function (event) {
		event.preventDefault();
		console.log("Register Attempted");
		try {
			const usernameAttempt = document.getElementById("username").value;
			const passwordAttempt = document.getElementById("password").value;

			const registerData = {
				username: usernameAttempt,
				password: passwordAttempt,
			};

			const response = await fetch("/auth/register", {
				method: "POST",
				headers: { "Content-Type": "application/json" },
				body: JSON.stringify(registerData),
			});

			if (!response.ok) {
				throw new Error("Failed to Register");
			}
		} catch (error) {
			console.error(error);
		}
	});
});

/* ----- Log Out ----- */
document.addEventListener("DOMContentLoaded", async function () {
	const loginButton = document.getElementById("logoutButton");

	loginButton.addEventListener("click", async function (event) {
		event.preventDefault();
		console.log("Logout Attempted");
		try {
			const response = await fetch("/auth/logout", {
				method: "GET",
				headers: { "Content-Type": "application/json" },
			});

			console.log(response);

			if (!response.ok) {
				throw new Error("Failed to Register");
			}
		} catch (error) {
			console.error(error);
		}
	});
});
