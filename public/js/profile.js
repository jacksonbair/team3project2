document.addEventListener("DOMContentLoaded", async function () {
	try {
		const response = await fetch("/workout/get", {
			method: "POST",
			headers: { "Content-Type": "application/json" },
		});

		console.log(response);

		const data = await response.json();

		printWorkouts(data);

		if (!response.ok) {
			throw new Error("Failed to Retrieve Workouts From Database");
		}
	} catch (error) {
		console.error(error);
	}
});

function printWorkouts(query) {
	const destination = document.getElementById("profile-display");

	let htmlContent = "";

	// console.log(query);
	for (let i = 0; i < query.length; i++) {
		let workout = query[i];

		// Create a div for each workout
		htmlContent += "<div class='workout'>";

		// Display workout details
		htmlContent +=
			"<p><strong>Workout Name:</strong> " + workout.name + "</p>" + "<br>";
		htmlContent +=
			"<p><strong>Workout Type:</strong> " + workout.type + "</p>" + "<br>";
		htmlContent +=
			"<p><strong>Muscle Group:</strong> " + workout.muscle + "</p>" + "<br>";
		htmlContent +=
			"<p><strong>Instructions:</strong> " +
			workout.instructions +
			"</p>" +
			"<br>";
		htmlContent +=
			"<p><strong>Equipment:</strong> " + workout.equipment + "</p>" + "<br>";
		htmlContent +=
			"<p><strong>Difficulty:</strong> " + workout.difficulty + "</p>" + "<br>";

		htmlContent +=
			"<button onclick='deleteWorkout(" +
			JSON.stringify(workout) +
			")'>Delete from Database</button>";

		// Close the workout div
		htmlContent += "</div>";

		// Add a horizontal line between workouts
		htmlContent += "<hr>";
	}

	destination.innerHTML = htmlContent;
}

async function deleteWorkout(workout) {
	try {
		const response = await fetch("/workout/remove", {
			method: "POST",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify(workout),
		});

		window.location.reload();

		if (!response.ok) {
			throw new Error("Failed to Delete Workout");
		}
	} catch (error) {
		console.error(error);
	}
}
