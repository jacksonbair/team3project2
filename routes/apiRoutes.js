import express from "express"; // import the express module
import { SECRET_API_KEY, SECRET_ACCESS_TOKEN } from "../config/index.js";

function redirectToProfile(callback) {
	let profileURL = "../html/profile.html";

	setTimeout(function () {
		window.location.href = profileURL;
	}, 1000);

	setTimeout(function () {
		if (callback && typeof callback === "function") {
			callback();
		}
	}, 1100);
}

export async function superTest() {
	console.log("superTest function called");
}

export async function getWorkouts(params, res) {
	// Construct API request URL
	var apiURL = "https://api.api-ninjas.com/v1/exercises?";

	Object.entries(params).forEach(([key, value]) => {
		if (value !== "" && value !== undefined) {
			apiURL += `${key}=${value}&`;
		}
	});

	// Remove the trailing "&" from the URL
	apiURL = apiURL.slice(0, -1);
	console.log(apiURL);
	try {
		const response = await fetch(apiURL, {
			method: "GET",
			headers: { "X-Api-Key": SECRET_API_KEY },
			contentType: "application/json",
		});

		if (!response.ok) {
			throw new Error("Failed to retrieve workouts");
		}

		const data = await response.json();
		console.log(data);
		return data;
	} catch (error) {
		console.error("Error:", error);
		throw error; // Re-throw the error to be caught by the calling function
	}
}

export default { getWorkouts, superTest };

// EXPORT ROUTE frontend javascript should access back end
