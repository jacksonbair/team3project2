import express from "express";
import { Register } from "../controllers/auth.js";
import Validate from "../middleware/validate.js";
import { check } from "express-validator";
import { Login } from "../controllers/auth.js";
import { Logout } from "../controllers/auth.js";

import path from "path";
import { fileURLToPath } from "url";
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const router = express.Router();

// Register route -- POST request
router.post(
	"/register",
	check("username")
		.not()
		.isEmpty()
		.withMessage("You username is required")
		.trim()
		.escape(),
	check("password")
		.not()
		.isEmpty()
		.isLength({ min: 8 })
		.withMessage("Must be at least 8 chars long"),
	Validate,
	Register
);

router.get("/logout", Logout);

router.post("/login", check("password").not().isEmpty(), Validate, Login);

export default router;
