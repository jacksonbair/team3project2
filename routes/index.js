import path from "path";
import { fileURLToPath } from "url";
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

import { getWorkouts, superTest } from "./apiRoutes.js";
import express from "express"; // import the express module
import auth from "./auth.js";
import workouts from "./workout.js";
import Verify from "../middleware/verify.js";

const app = express(); // Create an app object

app.use("/auth", auth);
app.use("/workout", workouts);

app.disable("x-powered-by"); // Reduce fingerprinting (optional)
// home route with the get method and a handler
app.get("/", (req, res) => {
	res.sendFile(path.join(__dirname, "../public/html/index.html"));
});

app.get("/user", Verify, (req, res) => {
	res.status(200).json({
		status: "success",
		message: "Welcome to the your Dashboard!",
	});
});

app.get("/account", Verify, (req, res) => {
	// if cookie returns a user, direct to that user's profile page
	// Otherwise direct to the login/register page
	res.sendFile(path.join(__dirname, "../public/html/profile.html"));
});

app.post("/workouts", async (req, res) => {
	try {
		const queryParams = req.body;
		const data = await getWorkouts(queryParams); // Call getWorkouts function to retrieve the data
		res.json(data); // Send the retrieved data as the response
	} catch (error) {
		console.error("Error fetching workouts", error);
		res.status(500).json({ error: "Internal Server Error" });
	}
});

app.get("/test", (req, res) => {
	res.sendFile(path.join(__dirname, "../public/html/index.html"));
});

app.post("/test", (req, res) => {
	superTest();
	res.sendFile(path.join(__dirname, "../public/html/index.html"));
});

export default app;
