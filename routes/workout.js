import express from "express";
const router = express.Router();
import {
	addWorkout,
	getWorkouts,
	removeWorkout,
} from "../controllers/workout.js";
import Verify from "../middleware/verify.js";

router.post("/add", Verify, (req, res) => {
	addWorkout(req, res);
});

router.post("/get", Verify, (req, res) => {
	getWorkouts(req, res);
});

router.post("/remove", Verify, (req, res) => {
	removeWorkout(req, res);
});

export default router;
