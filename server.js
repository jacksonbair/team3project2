import express from "express";
import cors from "cors";
import cookieParser from "cookie-parser";
import mongoose from "mongoose";
import { PORT, URI } from "./config/index.js";
import App from "./routes/index.js";

// === 1 - CREATE SERVER ===
const app = express();

// CONFIGURE HEADER INFORMATION
// Allow request from any source. In real production, this should be limited to allowed origins only
app.use(cors());
app.disable("x-powered-by"); //Reduce fingerprinting
app.use(cookieParser());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(express.static("public"));

// === 2 - CONNECT DATABASE ===
// Set up mongoose's promise to global promise
mongoose.promise = global.Promise;
mongoose.set("strictQuery", false);
mongoose
	.connect(URI)
	.then(console.log("Connected to database"))
	.catch((err) => console.log(err));

// === 4 - CONFIGURE ROUTES ===
// Connect Main route to app
app.use(App);

// === 5 - START UP SERVER ===
app.listen(PORT, () => console.log(`Server running on localhost port ${PORT}`));
